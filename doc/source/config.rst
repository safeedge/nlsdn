Configuration
=============

An ``nlsdn`` config file is a JSON dictionary containing subsections,
each of which is itself a dictionary.  There is an example config file
in the source tree in ``etc/config.json``.  The following subsections
briefly describe each subsection and its possible values.

(Note: where file paths are specified as config values, they may be
absolute paths, or relative paths.  If the latter, they are interpreted
relative to the config file's location.)

``server``
----------

First, this section is only relevant if running in standalone mode
(e.g., by executing ``$PREFIX/bin/nlsdn-flask``), rather than from a web
server via WSGI.  You can choose the bind address by setting ``host`` to
an IP address or hostname.  You can choose the listen port by setting
``port`` to an integer.

``log``
-------

``nlsdn`` provides many logging options.  You can set ``stderr`` to
``true`` to log to standard error.  You can set ``syslog`` to ``true``
to log to syslog via the default facility, or you can set it to a string
that is the lowercase name for one of the standard system facilities.
You can set ``file`` to a file, and ``nlsdn`` will append log entries to
that file.

You can also control the amount and type of information logged using
standard Python log level names.  Set the ``level`` and ``flask_level``
keys to one of those log level names (see
https://docs.python.org/2/library/logging.html#logging-levels).

``tls``
-------

If you are running standalone, you will certainly want to configure the
server with SSL/TLS.  The ``server_privkey`` key should be set to a file
path that contains a PEM-formatted private key.  The ``server_cert`` key
should be set to a file path that contains a PEM-formatted certificate.
Finally, the ``ca_cert`` key should be set to a file path that contains
a PEM-formatter certificate authority certificate, to support
self-signed CAs, or those not present in the system CA chain.

If you want to quickly generate an insecure self-signed CA cert, key, and
cert, you can do something like this:

.. code-block:: shell

    openssl genrsa -out ca.key 2048
    openssl req -x509 -new -nodes -key ca.key -sha256 -days 10365 -out ca.pem \
        -subj "/C=US/ST=Utah/L=Salt Lake City/O=University of Utah/OU=Flux Research Group/CN=My Self-Signed CA"
    openssl genrsa -out server.key 2048
    openssl req -new -key server.key -out server.csr \
        -subj "/C=US/ST=UT/L=Salt Lake City/O=University of Utah/OU=Flux Research Group/CN=localhost"
    openssl x509 -req -in server.csr -CA ca.pem -CAkey ca.key \
        -CAcreateserial -out server.pem -days 10365 -sha256

``auth``
--------

Because you are exposing direct control of your host's network stack,
you should enable authentication by setting the ``enabled`` key to
``true``.  Then you must add username and password key/value pairs to
the dictionary value of the ``users`` key.

``nlsdn``
---------

This subsection specifies the details of ``nlsdn``'s netlink and SDN
behavior.  For instance, the ``rule_range`` key must be assigned a
two-member list value that defines a range of rule indicies the SDN
engine should use to implement matching.  Rules point into tables that
contain SDN actions.  The ``table_range`` key must be assigned another
two-member list value that defines the range of table numbers that the
SDN engine is allowed to use.

``nl_endpoint``
---------------

You can enable or disable the netlink raw API endpoint by setting the
``enabled`` key to ``true`` or ``false``.  You can set its access URL by
setting the ``url`` key; this defaults to ``/service/nl/v1``.

``nlsdn_endpoint``
------------------

You can enable or disable the higher-level SDN API endpoint by setting the
``enabled`` key to ``true`` or ``false``.  You can set its access URL by
setting the ``url`` key; this defaults to ``/service/nlsdn/v1``.
