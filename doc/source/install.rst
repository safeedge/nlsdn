Installation
============

``nlsdn`` requires ``pyroute2`` and ``flask``.  We've made some small
modifications to ``pyroute2`` at
https://gitlab.flux.utah.edu/safeedge/pyroute2 .  If those are important
to you, you can install ``pyroute2`` from that repository, by running

    ``$ python setup.py install --user``

Otherwise, if you are running Ubuntu, you can install via

    ``$ apt-get install python-pyroute2 python-flask``.

You can install `nlsdn` in the normal way:

    ``$ python setup.py install --user``

or

    ``$ python setup.py install --prefix=/usr/local``

or similar.  If you use the ``--user`` option, `nlsdn` will be
installed in ``$HOME/.local/{bin,lib}`` or thereabouts.

You can build the `nlsdn` documentation via

    ``$ python setup.py build_sphinx``

The documentation will be available in `doc/build/html`.
