#!/bin/sh

set -x

export OURDIR=/root/setup
export SRC=/local/repository
export PSRC=${SRC}/tests/cloudlab-profile

cd $PSRC
for script in setup-install.sh setup-ipv6.py setup-nlsdn-bind-iface.py ; do
    ./$script | tee - $OURDIR/${script}.log 2>&1
done

# Fire it off!
systemctl daemon-reload
systemctl enable nlsdn
systemctl restart nlsdn
