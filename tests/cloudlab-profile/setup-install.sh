#!/bin/sh

set -x

# Exit if we've already done this.
if [ -e /etc/systemd/system/nlsdn.service ]; then
    exit 0
fi

. /etc/lsb-release

if [ "$DISTRIB_ID" = "Ubuntu" ]; then
    # Setup apt-get to not prompt us
    echo "force-confdef" > /etc/dpkg/dpkg.cfg.d/cloudlab
    echo "force-confold" >> /etc/dpkg/dpkg.cfg.d/cloudlab
    export DEBIAN_FRONTEND=noninteractive
    # -o Dpkg::Options::="--force-confold" -o Dpkg::Options::="--force-confdef" 
    DPKGOPTS=''
    APTGETINSTALLOPTS='-y'
    APTGETINSTALL="apt-get $DPKGOPTS install $APTGETINSTALLOPTS"
    apt-get update

    $APTGETINSTALL python-flask python-setuptools

    git clone https://gitlab.flux.utah.edu/safeedge/pyroute2
    cd pyroute2
    python setup.py install
    cd ..
else
    echo "ERROR: unsupported distribution '$DISTRIB_ID'"
    exit 1
fi

cd $SRC
python setup.py install

# Do some post-install stuff.
mkdir -p /etc/nlsdn/certs
chmod 700 /etc/nlsdn/certs
cp -p $SRC/etc/config.json /etc/nlsdn/
chown root:root /etc/nlsdn/config.json
chmod 660 /etc/nlsdn/config.json
cp -p $SRC/tests/certs/* /etc/nlsdn/certs/
chmod 600 /etc/nlsdn/certs/*.key
cp -p $SRC/etc/nlsdn.service /etc/systemd/system
