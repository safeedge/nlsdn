#!/usr/bin/env python

import geni.portal as portal
import geni.rspec.pg as RSpec
import geni.rspec.igext as IG
# Emulab specific extensions.
import geni.rspec.emulab as emulab
import geni.namespaces as GNS
from lxml import etree as ET
import random
import os.path
import sys
import struct
import socket

def mask2prefixlen(x):
    bits = 32
    if isinstance(x,basestring):
        if x.find(":") > -1:
            (xh,xl) = struct.unpack('!QQ',socket.inet_pton(socket.AF_INET6,x))
            x = (xh << 64) | xl
            bits = 128
        else:
            (x,) = struct.unpack('!L',socket.inet_pton(socket.AF_INET,x))
    else:
        if x > 2**32:
            bits = 128
    ret = 0
    while bits > 0:
        #print "x = %s, ret = %s, bits = %s" % (str(x),str(ret),str(bits))
        if ret and (x & 1) == 0:
            raise Exception("invalid netmask")
        ret += x & 1
        x = x >> 1
        bits -= 1
    return ret

def prefixlen2mask(n,protocol=4):
    is6 = False
    if n > 32 or protocol == 6 or protocol == 'ipv6':
        sl = 128 - n
        is6 = True
    else:
        sl = 32 - n
    x = 0
    while n > 0:
        x <<= 1
        x |= 1
        n -= 1
    if sl > 0:
        x <<= sl
    #print hex(x), x
    if is6:
        enc = struct.pack('!QQ',x >> 64,x & (2**64 - 1))
        return socket.inet_ntop(socket.AF_INET6,enc)
    else:
        enc = struct.pack('!L',x)
        return socket.inet_ntop(socket.AF_INET,enc)
    return x

class IPv4Address(RSpec.Address):
    def __init__ (self, address, netmask=None, prefixlen=None):
        if netmask is None and prefixlen is None:
            raise Exception("no mask information")
        elif netmask:
            prefixlen = mask2prefixlen(netmask,protocol=4)
        else:
            netmask = prefixlen2mask(prefixlen,protocol=4)
        super(IPv4Address, self).__init__("ipv4")
        self.address = address
        self.netmask = netmask
        self.prefixlen = prefixlen

    def _write (self, element):
        ip = ET.SubElement(element, "{%s}ip" % (GNS.REQUEST.name))
        ip.attrib["address"] = self.address
        ip.attrib["netmask"] = self.netmask
        ip.attrib["prefixlen"] = str(self.prefixlen)
        ip.attrib["type"] = self.type
        return ip

class IPv6Address(RSpec.Address):
    def __init__ (self, address, netmask=None, prefixlen=None):
        if netmask is None and prefixlen is None:
            raise Exception("no mask information")
        elif netmask:
            prefixlen = mask2prefixlen(netmask,protocol=4)
        else:
            netmask = prefixlen2mask(prefixlen,protocol=4)
        super(IPv6Address, self).__init__("ipv6")
        self.address = address
        self.netmask = netmask
        self.prefixlen = prefixlen

    def _write (self, element):
        ns = "http://www.protogeni.net/resources/rspec/ext/johnsond/1"
        ip = ET.SubElement(element, "{%s}ip" % (ns,))
        ip.attrib["address"] = self.address
        ip.attrib["netmask"] = self.netmask
        ip.attrib["prefixlen"] = str(self.prefixlen)
        ip.attrib["type"] = self.type
        return ip

CMD = "sudo mkdir -p /root/setup && sudo -H /local/repository/tests/cloudlab-profile/setup.sh 2>&1 | sudo tee /root/setup/setup.log"

nodemap = {
    '2':1,'7':1,'8':1,'10':1,'cb':1,'7se':1,'10se':1,
    'sdx':2,'5':2,'9':2,'11':2,'12':2,'co':2,'11se':2 }
mgmtmap = {
    '2':2,'7':7,'8':8,'10':10,'cb':16,'7se':17,'10se':18,
    'sdx':20,'5':5,'9':9,'11':11,'12':12,'co':15,'11se':19 }

mgmtprefix = "10.60.0"
mgmtbits = 24
prefix4 = "192.168.0"
bits4 = 30
prefix6 = "2620:007c:d000:ffff"
bits6 = 126
addrs = {
    "node-2-cb":0x1,"node-2-7":0x5,"node-2-10":0x9,"node-2-8":0xd,
    "node-5-9":0x11,"node-5-11":0x15,"node-5-12":0x19,"node-5-sdx":0x4a,
    "node-5-cb-1":0x58,"node-5-cb-2":0x5c,
    "node-7-2":0x6,"node-7-cb":0x1d,"node-7-10":0x21,"node-7-7se":0x54,
    "node-8-2":0xe,"node-8-cb":0x25,"node-8-10":0x29,
    "node-9-12":0x31,"node-9-11":0x2d,"node-9-5":0x12,"node-9-co":0x35,
    "node-10-8":0x2a,"node-10-7":0x22,"node-10-2":0xa,"node-10-10se":0x50,
    "node-11-9":0x2e,"node-11-5":0x16,"node-11-co":0x39,"node-11-11se":0x4c,
    "node-12-9":0x32,"node-12-5":0x1a,"node-12-co":0x3d,
    "node-co-11":0x3a,"node-co-9":0x36,"node-co-12":0x3e,
    "node-cb-2":0x2,"node-cb-8":0x26,"node-cb-7":0x1e,"node-cb-sdx":0x46,
    "node-cb-5-1":0x59,"node-cb-5-2":0x5d,
    "node-7se-7":0x55,
    "node-10se-10":0x51,
    "node-11se-11":0x4d,
    "node-sdx-cb":0x45,"node-sdx-5":0x49,
}

pc = portal.Context()

pc.defineParameter(
    "hostType","Physical Node Type",portal.ParameterType.NODETYPE,"d430",
    [("any","Any"),("d430","d430 (64GB)"),("d820","d820 (128GB)"),
     ("xl170","xl170 (64GB)"),("m510","m510 (64GB)")],
    longDescription="A specific hardware type to use for each physical node.  If you choose Any, the profile will allow Cloudlab to map the VMs to physical hosts using its resource mapper.  If you select a node type, you must ensure that type has enough memory to host 7 VMs with the amount of cores and memory in the next parameter, minus 4GB for the hypervisor.  (Cloudlab clusters all have machines of specific types.  When you set this field to a value that is a specific hardware type, you will only be able to instantiate this profile on clusters with machines of that type.  If unset, when you instantiate the profile, the resulting experiment may have machines of any available type allocated.)")
pc.defineParameter(
    "coresPerVM","Cores per VM",portal.ParameterType.INTEGER,1,
    longDescription="Number of cores each VM will get.")
pc.defineParameter(
    "ramPerVM","RAM per VM",portal.ParameterType.INTEGER,4096,
    longDescription="Amount of RAM each VM will get in MB.")
pc.defineParameter(
    "vmImage","VM Image",portal.ParameterType.STRING,
    'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD',
    longDescription="The image your VMs will run.")
pc.defineParameter(
    "hostImage","Host Image",portal.ParameterType.STRING,
    'urn:publicid:IDN+emulab.net+image+emulab-ops//XEN49-64-STD',
    longDescription="The image your VM host nodes will run, if you specify a physical host node type above.")
pc.defineParameter(
    "linkSpeed","Experiment Link Speed",portal.ParameterType.INTEGER,0,
    [(0,"Any"),(1000000,"1Gb/s"),(10000000,"10Gb/s")],
    longDescription="A specific link speed to use for each node.  All experiment network interfaces will request this speed.")
pc.defineParameter(
    "linkLatency","LAN/Link Latency",portal.ParameterType.LATENCY,0,
    longDescription="A specific latency to use for each LAN and link.")
#pc.defineParameter(
#    "linkLoss","LAN/Link Loss",portal.ParameterType.FLOAT,0.0,
#    longDescription="A specific loss rate to use for each LAN and link.")
pc.defineParameter(
    "multiplex","Multiplex Networks",portal.ParameterType.BOOLEAN,True,
    longDescription="Multiplex all LANs and links.")
pc.defineParameter(
    "bestEffort","Best Effort Link Bandwidth",portal.ParameterType.BOOLEAN,True,
    longDescription="Do not require guaranteed bandwidth throughout switch fabric.")
#pc.defineParameter(
#    "trivialOk","Trivial Links Ok",portal.ParameterType.BOOLEAN,True,
#    longDescription="Maybe use trivial links.")
#pc.defineParameter(
#    "forceVlanLinkType","Force VLAN Link Type",portal.ParameterType.BOOLEAN,False,
#    longDescription="Force the type of each LAN/Link to vlan.")

params = pc.bindParameters()

if params.coresPerVM < 1:
    pc.reportError(portal.ParameterError(
        "Must specify at least one core per VM",['coresPerVM']))
if params.ramPerVM < 1:
    pc.reportError(portal.ParameterError(
        "Must specify at least one core per VM",['ramPerVM']))

rspec = RSpec.Request()

tour = IG.Tour()
tour.Description(
    IG.Tour.TEXT,
    "Create an experiment emulating the Ammon/Entrypoint deployment, and do some SDN stuff via netlink, using the nlsdn application.")
rspec.addTour(tour)

vhosts = {}
nodes = {}
links = {}

mgmtlan = RSpec.LAN('mgmt-lan')
if params.multiplex:
    mgmtlan.link_multiplexing = True
    # Need this cause LAN() sets the link type to lan, not sure why.
    mgmtlan.type = "vlan"
if params.bestEffort:
    mgmtlan.best_effort = True

for (k,v) in addrs.iteritems():
    sa = k.split('-')[1:]
    (src,dst) = (sa[0],sa[1])
    if len(sa) > 2:
        linkidx = int(sa[2])
        postfix = "-" + sa[2]
    else:
        linkidx = None
        postfix = ""
    srcname = 'node-' + src
    dstname = 'node-' + dst
    for (name,num) in [(srcname,src),(dstname,dst)]:
        if name in nodes:
            continue
        vnode = nodes[name] = IG.XenVM(name)
        vnode.addService(RSpec.Execute(shell="sh",command=CMD))
        vnode.cores = params.coresPerVM
        vnode.ram = params.ramPerVM
        vnode.exclusive = True
        if params.vmImage:
            vnode.disk_image = params.vmImage
        if params.hostType != "any":
            vhostnum = nodemap[num]
            vhostname = "vhost%s" % (str(vhostnum),)
            vnode.InstantiateOn(vhostname)
            if not vhostname in vhosts:
                vhost = vhosts[vhostname] = RSpec.RawPC(vhostname)
                vhost.exclusive = True
                if params.hostType:
                    vhost.hardware_type = params.hostType
                if params.hostImage:
                    vhost.disk_image = params.hostImage
        iface = vnode.addInterface("ifM")
        iface.addAddress(
          IPv4Address("%s.%s" % (mgmtprefix,mgmtmap[num]),prefixlen=mgmtbits))
        mgmtlan.addInterface(iface)
    linkname = "link-%s-%s%s" % (src,dst,postfix)
    revlinkname = "link-%s-%s%s" % (dst,src,postfix)
    if not linkname in links and not revlinkname in links:
        links[linkname] = link = RSpec.Link(linkname)
        if params.linkSpeed > 0:
            link.bandwidth = int(params.linkSpeed)
        if params.linkLatency > 0:
            link.latency = int(params.linkLatency)
        if params.multiplex:
            link.link_multiplexing = True
            link.type = "vlan"
        if params.bestEffort:
            link.best_effort = True
            
        iface = nodes[srcname].addInterface("%s%s" % (dst,postfix))
        sdnum = addrs["node-%s-%s%s" % (src,dst,postfix)]
        iface.addAddress(
          IPv4Address("%s.%s" % (prefix4,str(sdnum)),prefixlen=bits4))
        iface.addAddress(
          IPv6Address("%s::%s" % (prefix6,hex(sdnum)[2:]),prefixlen=bits6))
        link.addInterface(iface)
        iface = nodes[dstname].addInterface("%s%s" % (src,postfix))
        dsnum = addrs["node-%s-%s%s" % (dst,src,postfix)]
        iface.addAddress(
          IPv4Address("%s.%s" % (prefix4,str(dsnum)),prefixlen=bits4))
        iface.addAddress(
          IPv6Address("%s::%s" % (prefix6,hex(dsnum)[2:]),prefixlen=bits6))
        link.addInterface(iface)

for vh in vhosts.keys():
    rspec.addResource(vhosts[vh])
for nn in nodes.keys():
    rspec.addResource(nodes[nn])
rspec.addResource(mgmtlan)
for ln in links.keys():
    rspec.addResource(links[ln])

pc.printRequestRSpec(rspec)
