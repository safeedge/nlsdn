``nlsdn`` (Linux Netlink SDN-like control)
==========================================

``nlsdn`` (https://gitlab.flux.utah.edu/safeedge/nlsdn)
is a simple RESTful web application that allows its users to
programmably control features of a Linux host's networking stack
via netlink, using ``pyroute2``.  It provides both a low-level
"pure" netlink API, as well as a higher-level SDN-like northbound API.

The ``nlsdn`` server can be configured to run from a web server
via WSGI, or can be run standalone, if the selected web framework
permits (for now, only Flask is supported, but others are possible).
For any serious application, you should run it via WSGI;
however, the standalone version is multithread-safe.

You can view the latest documentation at
http://safeedge.pages.flux.utah.edu/nlsdn .

``nlsdn`` supports Python 2 on Linux.
