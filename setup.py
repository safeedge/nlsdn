#!/usr/bin/env python

from setuptools import setup, find_packages
from codecs import open
import os.path
import sys

here = os.path.abspath(os.path.dirname(__file__))

# Get the long description from the README file
with open(os.path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='nlsdn',
    version='0.1.0',
    author='David M Johnson',
    author_email='johnsond@flux.utah.edu',
    url='https://gitlab.flux.utah.edu/safeedge/nlsdn',
    description='A library and standalone/WSGI Linux Netlink SDN control interface',
    long_description=long_description,
    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Server',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'License :: OSI Approved :: GNU Affero General Public License v3',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    keywords='linux netlink sdn software-defined networking segment routing',
    package_dir={"": "src"},
    packages=find_packages(where="src"),
    install_requires=['pyroute2','flask','cryptography',],
    #extra_requires=[],
    #
    # We do not use console_scripts entry_points because they insist on
    # triggering the entrypoint iff __name__ == "__main__"... and of
    # course that doesn't work for WSGI entrypoints (i.e. from an
    # external webserver).
    #
    #entry_points={
    #    'console_scripts': [
    #        'nlsdn-flask-standalone=nlsdn.app.app_flask:main',
    #        'nlsdn-flask-wsgi=nlsdn.app.app_flask:wsgi',
    #    ],
    #},
    scripts=[ 'bin/nlsdn-flask' ],
    #include_package_data=True,
)
