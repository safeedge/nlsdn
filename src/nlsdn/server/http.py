import base64
import json
import pyroute2
from cryptography.hazmat.backends import default_backend
from cryptography.x509 import load_pem_x509_certificate
from nlsdn.log import LOG
from nlsdn.util import CustomJsonEncoder

class InvalidRequestError(Exception):
    pass

class BaseRequest(object):
    """
    A simple HTTP request adapter class that provides uniform access to
    various bits in the request which
    :class:`nlsdn.server.handlers.NlSdnServer` requires.  Functions as
    an adapter between the web framework in use (i.e., Flask, Twisted)
    and :class:`nlsdn.server.handler.NlSdnServer` request handling.
    """
    def __init__(self,method,uri,client_ip,
                 args=dict({}),data=None,headers=dict({}),environ=dict({}),
                 framework_request_obj=None):
        """
        :param method: the HTTP method.
        :param uri: the original, full URI.
        :param client_ip: the client's IP as a string.
        :param args: a py:type:`dict` of key/value pairs, where the values might be lists if the parameter is multi-valued.
        :param headers: a py:type:`dict` of HTTP headers.
        :param environ: a py:type:`dict` of the server environment.
        :param framework_request_obj: the framework's original Request object.
        """
        self.method = method
        self.uri = uri
        self.client_ip = client_ip
        self.args = args
        self.data = data
        self.headers = headers
        self.environ = environ
        self._framework_request_obj = framework_request_obj

    def __repr__(self):
        return "BaseRequest(%s,%s,%s args=%d,headers=%d,env=%d)" \
          % (self.method,self.client_ip,self.uri,
             len(self.args),len(self.headers),len(self.environ))

class BasicAuthRequestMixin(object):
    """
    A request mixin that extracts HTTP Basic Auth username and password
    and presents them as properties.  py:type:`NoneType` usernames and
    passwords are allowed!
    """
    
    @property
    def authorization(self):
        if hasattr(self,'_authorization'):
            return self._authorization
        authstring = self.headers.get("Authorization")
        if not authstring:
            return None
        setattr(self,'_authorization',authstring)
        return authstring

    def __extract_basic_auth(self):
        authstring = self.authorization
        if not authstring:
            raise InvalidRequestError("no Authorization header")
        [auth_type,auth_blob] = authstring.split(' ',1)
        if auth_type != "basic" and auth_type != "Basic":
            LOG.debug("%r: not using HTTP basic auth (%s)!",
                      self,auth_type)
            raise InvalidRequestError("not using HTTP Basic auth")
        bd = base64.b64decode(auth_blob)
        #
        # In python2, returns a binary str; in python3, returns a bytes.
        #
        if not isinstance(bd,str):
            bd = str(bd,'utf8')
        [username,password] = bd.split(":",1)
        return (username,password)
    
    @property
    def username(self):
        if hasattr(self,'_username'):
            return self._username
        try:
            (username,password) = self.__extract_basic_auth()
        except Exception:
            return None
        setattr(self,'_username',username)
        setattr(self,'_password',password)
        return username
        
    @property
    def password(self):
        if hasattr(self,'_password'):
            return self._password
        try:
            (username,password) = self.__extract_basic_auth()
        except Exception:
            return None
        setattr(self,'_username',username)
        setattr(self,'_password',password)
        return password

class CertRequestMixin(object):
    """
    A request mixin that extracts client x.509 certificates from
    commonly-used webserver and WSGI environments, and presents them as
    properties.
    """
    
    @property
    def client_cert(self):
        if hasattr(self,'_client_cert'):
            return self._client_cert
        raw = self.environ.get("SSL_CLIENT_CERT")
        if raw is None or raw == "":
            return None
        cc = load_pem_x509_certificate(raw,default_backend())
        setattr(self,'_client_cert',cc)
        return cc
    
    @property
    def client_cert_verified(self):
        if hasattr(self,'_client_cert_verified'):
            return self._client_cert_verified
        v = self.environ.get("SSL_CLIENT_VERIFY")
        if v is None or v == "":
            return False
        if v == "SUCCESS":
            v = True
        else:
            v = False
        setattr(self,'_client_cert_verified',v)
        return v

class NlSdnRequest(BaseRequest,BasicAuthRequestMixin,CertRequestMixin):
    """
    A basic netlink sdn request class that extends the
    :class:`BaseRequest` with several mixins.
    """
    def __init__(self,subpath,method,uri,client_ip,
                 args=dict({}),data=None,headers=dict({}),environ=dict({}),
                 framework_request_obj=None):
        super(NlSdnRequest,self).__init__(
            method,uri,client_ip,
            args=args,data=data,headers=headers,environ=environ,
            framework_request_obj=framework_request_obj)
        self.subpath = subpath

    def __repr__(self):
        return "NlSdnRequest(%s,%s,%s,%s %s)" \
          % (self.subpath,self.method,self.client_ip,self.uri,self.username)

class Response(object):

    def __init__(self,code,content="",headers={}):
        self.code = code
        self.content = content
        self.headers = headers

    def __repr__(self):
        return "Response(%d,len(content)=%d,len(headers)=%d)" \
            % (self.code,len(self.content),len(self.headers.keys()))

class JsonResponse(Response):
    def __init__(self,code,blob,headers={},minimal=False):
        if headers is not None:
            nh = dict(headers)
        else:
            nh = dict()
        nh['Content-Type'] = 'application/json'
        if blob is None:
            content = ""
        else:
            if not minimal:
                content = json.dumps(blob,cls=CustomJsonEncoder)
            else:
                content = json.dumps(
                    blob,indent=None,separators=(',', ':'),
                    cls=CustomJsonEncoder)
        super(JsonResponse,self).__init__(
            code,content=content,headers=nh)

class JsonErrorResponse(JsonResponse):
    def __init__(self,code,errorblob,headers={},minimal=False):
        blob = dict(error=errorblob)
        super(JsonErrorResponse,self).__init__(
            code,blob,headers=headers,minimal=minimal)

class JsonAuthenticateChallengeResponse(JsonErrorResponse):
    def __init__(self):
        super(JsonAuthenticateChallengeResponse,self).__init__(
            401,"authentication required",
            headers={'WWW-Authenticate':'Basic'})

class JsonInternalServerErrorResponse(JsonErrorResponse):
    def __init__(self,message=''):
        m = "Internal server error"
        if message:
            m = message
        super(JsonInternalServerErrorResponse,self).__init__(500,m)
