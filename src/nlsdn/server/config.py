
import os
import logging
from cryptography.x509 import load_pem_x509_certificate
from cryptography.hazmat.primitives.serialization \
    import load_pem_private_key
from cryptography.hazmat.primitives.serialization \
    import PublicFormat
from cryptography.hazmat.backends import default_backend
from nlsdn.config import (
    global_config_section, ConfigSection, ConfigError )

@global_config_section
class ServerConfig(ConfigSection):
    name = "server"

    def __init__(self,configdir,jsonblob):
        super(ServerConfig,self).__init__(configdir,jsonblob)
        if "threaded" in jsonblob:    self.threaded = jsonblob["threaded"]
        else:                         self.threaded = True
        if "host" in jsonblob:        self.host = jsonblob["host"]
        else:                         self.host = "localhost"
        if "port" in jsonblob:        self.port = jsonblob["port"]
        else:                         self.port = 5001
        if "user" in jsonblob and jsonblob["user"]:
            try:
                i = int(jsonblob["user"])
                pwent = pwd.getpwuid(i)
                self.uid = pwent.pw_uid
                self.user = jsonblob["user"]
            except Exception:
                try:
                    pwent = pwd.getpwnam(jsonblob["user"])
                    self.uid = pwent.pw_uid
                    self.user = jsonblob["user"]
                except Exception:
                    raise ConfigError("could not find user '%s'" % (jsonblob["user"]))
        else:
            self.uid = None
            self.user = None
        if "group" in jsonblob and jsonblob["group"]:
            try:
                i = int(jsonblob["group"])
                grent = grp.getgrgid(i)
                self.gid = grent.gr_gid
                self.group = jsonblob["group"]
            except Exception:
                try:
                    grent = grp.getgrnam(jsonblob["group"])
                    self.gid = grent.gr_gid
                    self.group = jsonblob["group"]
                except Exception:
                    raise ConfigError("could not find group '%s'" % (jsonblob["group"]))
        else:
            self.gid = None
            self.group = None

@global_config_section
class LogConfig(ConfigSection):
    name = "log"

    def __init__(self,configdir,jsonblob={}):
        super(LogConfig,self).__init__(configdir,jsonblob)
        if "stderr" in jsonblob:
            self.stderr = bool(jsonblob["stderr"])
        else:
            self.stderr = False
        if "syslog" in jsonblob:
            self.syslog = jsonblob["syslog"]
        else:
            self.syslog = "syslog"
        if "file" in jsonblob:
            self.file = jsonblob["file"]
            if not os.access(self.file,os.W_OK):
                raise ConfigError(
                    "cannot write to log file '%s'" % (self.file,))
        else:
            self.file = None
        if "level" in jsonblob:
            l = jsonblob["level"]
            if l == "error" or l == "ERROR":
                self.level = logging.ERROR
            elif l == "warning" or l == "WARNING":
                self.level = logging.WARNING
            elif l == "info" or l == "INFO":
                self.level = logging.INFO
            elif l == "debug" or l == "DEBUG":
                self.level = logging.DEBUG
            else:
                raise ConfigError("unknown log level '%s'; must be one of"
                                  " error,warning,info,debug" % (self.level))
        else:
            self.level = logging.WARNING
        if "flask_level" in jsonblob:
            l = jsonblob["flask_level"]
            if l == "error" or l == "ERROR":
                self.flask_level = logging.ERROR
            elif l == "warning" or l == "WARNING":
                self.flask_level = logging.WARNING
            elif l == "info" or l == "INFO":
                self.flask_level = logging.INFO
            elif l == "debug" or l == "DEBUG":
                self.flask_level = logging.DEBUG
            else:
                raise ConfigError(
                    "unknown log flask_level '%s'; must be one of"
                    " error,warn,info,debug" % (self.flask_level))
        else:
            self.flask_level = logging.WARNING

    def __eq__(self,other):
        if type(self) == type(other) \
            and self.syslog == other.syslog \
            and self.file == other.file \
            and self.level == other.level \
            and self.flask_level == other.flask_level:
            return True
        return False

@global_config_section
class TlsConfig(ConfigSection):
    name = "tls"

    def __init__(self,configdir,jsonblob):
        super(TlsConfig,self).__init__(configdir,jsonblob)
        if "server_privkey_passphrase" in jsonblob:
            self.server_privkey_passphrase = jsonblob["server_privkey_passphrase"]
        else:
            self.server_privkey_passphrase = None
        if "server_privkey" in jsonblob:
            try:
                filename = jsonblob["server_privkey"]
                if filename[0] != '/':
                    filename = self.configdir + "/" + filename
                fd = open(filename,'rb')
                contents = fd.read()
                fd.close()
                self.server_privkey = load_pem_private_key(
                    contents,self.server_privkey_passphrase,default_backend())
                self.server_privkey_file = filename
            except Exception as err:
                raise ConfigError("could not read 'server_privkey' (%s): %s"
                                  % (filename,str(err)))
        else:
            self.server_privkey = None
            self.server_privkey_file = None
        if "server_cert" in jsonblob:
            try:
                filename = jsonblob["server_cert"]
                if filename[0] != '/':
                    filename = self.configdir + "/" + filename
                fd = open(filename,'rb')
                contents = fd.read()
                fd.close()
                self.server_cert = load_pem_x509_certificate(contents,default_backend())
                self.server_cert_file = filename
            except Exception as err:
                raise ConfigError("could not read 'server_cert' (%s): %s"
                                  % (filename,str(err)))
        else:
            self.server_cert = None
            self.server_cert_file = None
        if "ca_cert" in jsonblob:
            try:
                filename = jsonblob["ca_cert"]
                if filename[0] != '/':
                    filename = self.configdir + "/" + filename
                fd = open(filename,'rb')
                contents = fd.read()
                fd.close()
                self.ca_cert = load_pem_x509_certificate(contents,default_backend())
                self.ca_cert_file = filename
            except Exception as err:
                raise ConfigError("could not read 'ca_cert' (%s): %s"
                                  % (filename,str(err)))
        else:
            self.ca_cert = None
            self.ca_cert_file = None

@global_config_section
class AuthConfig(ConfigSection):
    name = "auth"

    def __init__(self,configdir,jsonblob):
        super(AuthConfig,self).__init__(configdir,jsonblob)
        if "enabled" in jsonblob:
            self.enabled = bool(jsonblob["enabled"])
        else:
            self.enabled = True
        if "users" in jsonblob:
            if not isinstance(jsonblob["users"],dict):
                raise ConfigError(
                    "auth.users must be a dict of username/password pairs!")
            self.users = jsonblob["users"]
        else:
            self.users = {}

@global_config_section
class NetlinkEndpointConfig(ConfigSection):
    name = "nl_endpoint"

    def __init__(self,configdir,jsonblob):
        super(NetlinkEndpointConfig,self).__init__(configdir,jsonblob)
        if "url" in jsonblob:
            self.url = jsonblob["url"]
        else:
            self.url = "/service/netlink/v1"
        if "enabled" in jsonblob:
            self.enabled = bool(jsonblob["enabled"])
        else:
            self.enabled = False

@global_config_section
class NlSdnEndpointConfig(ConfigSection):
    name = "nlsdn_endpoint"

    def __init__(self,configdir,jsonblob):
        super(NlSdnEndpointConfig,self).__init__(configdir,jsonblob)
        if "url" in jsonblob:
            self.url = jsonblob["url"]
        else:
            self.url = "/service/nlsdn/v1"
        if "enabled" in jsonblob:
            self.enabled = bool(jsonblob["enabled"])
        else:
            self.enabled = False

@global_config_section
class NlSdnConfig(ConfigSection):
    name = "nlsdn"

    def __init__(self,configdir,jsonblob):
        super(NlSdnConfig,self).__init__(configdir,jsonblob)
        if "socket" in jsonblob:
            self.socket = jsonblob["socket"]
        else:
            self.socket = None
        if "rule_range" in jsonblob:
            self.rule_range = jsonblob["rule_range"]
            if not isinstance(self.rule_range,list) or \
              not len(self.rule_range) == 2 or \
              self.rule_range[0] < 0 or self.rule_range[0] > 32767 or \
              self.rule_range[1] < 0 or self.rule_range[1] > 32767 or \
              self.rule_range[0] >= self.rule_range[1]:
                raise ConfigError("nlsdn.rule_range invalid (%s): %s"
                                  % (filename,str(err)))
        else:
            raise ConfigError("nlsdn.rule_range must be specified (%s): %s"
                              % (filename,str(err)))
        if "table_range" in jsonblob:
            self.table_range = jsonblob["table_range"]
            if not isinstance(self.table_range,list) or \
              not len(self.table_range) == 2 or \
              self.table_range[0] < 0 or self.table_range[0] > 32767 or \
              self.table_range[1] < 0 or self.table_range[1] > 32767 or \
              self.table_range[0] >= self.table_range[1]:
                raise ConfigError("nlsdn.table_range invalid (%s): %s"
                                  % (filename,str(err)))
        else:
            raise ConfigError("nlsdn.table_range must be specified (%s): %s"
                              % (filename,str(err)))
        if "dbfile" in jsonblob:
            self.dbfile = jsonblob["dbfile"]
        else:
            self.dbfile = "/var/lib/nlsdn/db.json"
        if os.path.exists(self.dbfile) \
          and not os.access(self.dbfile,os.W_OK | os.R_OK):
            raise ConfigError(
                "cannot read/write nlsdn dbfile '%s'" % (self.dbfile,))
        elif not os.path.exists(os.path.dirname(self.dbfile)) \
          or not os.access(os.path.dirname(self.dbfile),os.W_OK | os.R_OK):
            raise ConfigError(
                "cannot read/write nlsdn dbfile dir '%s'" % (os.path.dirname(self.dbfile),))
