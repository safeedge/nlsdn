import os
import sys
import json
import itertools
import logging, logging.handlers
from past.builtins import basestring
from future.utils import iteritems
import pyroute2
from nlsdn.log import LOG
from nlsdn.server.http import (
    NlSdnRequest, Response, JsonResponse, JsonErrorResponse,
    JsonInternalServerErrorResponse, JsonAuthenticateChallengeResponse )
import nlsdn.server.config
import nlsdn.server.sdn

r = None

class NlSdnError(Exception):

    @property
    def message(self):
        return self.args[0]

class PublicNlSdnError(NlSdnError):
    """
    This error's contents are appropriate to share with the client.
    """

class NlSdnServer(object):
    """
    An NlSdnServer provides endpoints to talk to the local Netlink or to
    an SDN-over-Netlink abstraction, according to a `Config` object
    loaded from a config file.
    """
    def __init__(self,config,standalone=False):
        self.config = config
        self.standalone = standalone
        self.endpoints = []
        self.iproute = pyroute2.IPRoute()
        LOG.debug("NlSdnServer initial config:\n%s",self.config.dump())
        if config.nl_endpoint.enabled:
            self.endpoints.append(
                (config.nl_endpoint.url,"nl",self.handle_nl_request,
                 ['GET','POST','DELETE']))
        if config.nlsdn_endpoint.enabled:
            self.endpoints.append(
                (config.nlsdn_endpoint.url,"nlsdn",self.handle_nlsdn_request,
                 ['GET','POST','DELETE']))
            self.sdn = nlsdn.server.sdn.Engine(
                iproute=self.iproute,path=config.nlsdn.dbfile,
                rule_range=config.nlsdn.rule_range,
                action_range=config.nlsdn.table_range)
            self.sdn.load()
        self.setup_logging()

    def drop_privileges(self):
        """
        Drop privileges of this process according to the config.
        """
        uid = self.config.server.uid
        gid = self.config.server.gid
        if gid is not None and gid != 0:
            cgid = os.getegid()
            if cgid != gid:
                os.setgroups([])
                os.setgid(gid)
                LOG.info("dropped gid privilege to %d",os.getegid())
        if uid is not None and uid != 0:
            cuid = os.geteuid()
            if cuid != uid:
                os.setuid(uid)
                LOG.info("dropped uid privilege to %d",os.geteuid())

    def setup_logging(self):
        flask_modules = ['flask','flask.Flask','werkzeug','werkzeug.serving',
                         'pyroute2','pyroute2.netlink']
        procname = os.path.basename(sys.argv[0])

        if self.config.log.syslog:
            if isinstance(self.config.log.syslog,bool):
                nh = logging.handlers.SysLogHandler(address='/dev/log')
            else:
                nh = logging.handlers.SysLogHandler(
                    address='/dev/log',facility=self.config.log.syslog)
            fm = logging.Formatter(
                procname+"[%(process)d] %(name)s %(levelname)s %(message)s")
            nh.setFormatter(fm)
            LOG.addHandler(nh)
            for mod in flask_modules:
                logging.getLogger(mod).addHandler(nh)
        if self.config.log.file:
            nh = logging.FileHandler(self.config.log.file,mode='a')
            fm = logging.Formatter(
                "%(asctime)s [%(process)d] %(levelname)-8s %(pathname)s"
                ":%(lineno)d %(message)s")
            nh.setFormatter(fm)
            LOG.addHandler(nh)
            for mod in flask_modules:
                logging.getLogger(mod).addHandler(nh)
        if self.standalone and (self.config.log.stderr
                                or not self.config.log.file):
            # Once we open a console logger, it stays forever.
            nh = logging.StreamHandler()
            fm = logging.Formatter(
                "%(asctime)s [%(process)d] %(levelname)-8s %(pathname)s"
                ":%(lineno)d %(message)s")
            nh.setFormatter(fm)
            LOG.addHandler(nh)
            for mod in flask_modules:
                logging.getLogger(mod).addHandler(nh)
        LOG.setLevel(self.config.log.level)
        for mod in flask_modules:
            logging.getLogger(mod).setLevel(self.config.log.flask_level)

    def __validate_request(self,request,**kwargs):
        #
        # First, validate the request.
        #
        if not isinstance(request,NlSdnRequest):
            LOG.error("bug -- request (%r) must be subclass of NlSdnRequest",
                      request)
            return JsonInternalServerErrorResponse()
        resp = None

        LOG.debug("request %r",request)
        
        #
        # Second, try to authenticate.
        #
        if self.config.auth.enabled:
            try:
                if not request.authorization:
                    LOG.debug(
                        "request %r has no Authorization; challenging",
                        request)
                    #LOG.info("request headers: %s",str(request.headers))
                    return JsonAuthenticateChallengeResponse()
                if request.client_cert and not request.client_cert_verified:
                    LOG.warning("request %r client cert not validated!",
                                request)
                    return Response(401,"invalid certificate")
            except Exception:
                LOG.error("failed to validate request '%r'",request)
                LOG.exception(sys.exc_info()[1])
                return JsonInternalServerErrorResponse()

            try:
                username = request.username
                if username not in self.config.auth.users:
                    LOG.info("unknown user '%s'",username)
                    return Response(401,"unauthorized")
                elif self.config.auth.users[username] != request.password:
                    LOG.info("incorrect password for user '%s'",username)
                    return Response(401,"unauthorized")
                else:
                    LOG.info("authenticated user '%s'",username)
            except Exception:
                LOG.error("error in authentication")
                LOG.exception(sys.exc_info()[1])
                return JsonInternalServerErrorResponse()

    def _nl_get_iproute(self,netns=None):
        if netns == None:
            return self.iproute
        else:
            raise PublicNlSdnError("network namespaces not yet supported")

    def __filter_nl_value(self,l,f,keys,attrs):
        """
        Filters (and converts!) keys and attributes in l, if l is either
        a dict or list.  If f == 0 or out, remove named keys/attrs.  If
        f == 1 or in, remove keys/attrs not in the keys/attrs lists.  l
        may be a flat dict, or a list of dicts.

        Note that we *always* create new list element dicts, and copy the
        data in, instead of leaving it in the pyroute2 data structures.  For
        instance, for some structures, if you delete the `family` keyword,
        the structure is no longer able to decode and render nla_slot values,
        and they turn into garbage that we cannot encode as JSON.
        """
        inout = 0
        if f in [ 0,"0","out" ]:
            inout = 0
        elif f in [ 1,"1","in" ]:
            inout = 1

        LOG.debug("f=%s;k=%s;a=%s",str(f),str(keys),str(attrs))
        if l == None or \
          (not isinstance(l,dict) and not isinstance(l,list) and \
           not isinstance(l,tuple)):
            return l
        val = l
        if isinstance(l,dict):
            val = [ l ]
        nval = []
        for v in val:
            nv = {}
            for (k,v) in iteritems(v):
                if not keys or (inout == 0 and k not in keys) \
                  or (inout == 1 and k in keys):
                    nv[k] = v
            if 'attrs' in nv and isinstance(nv['attrs'],list):
                nattrs = []
                for av in nv['attrs']:
                    if isinstance(av,pyroute2.netlink.nla_slot) \
                      or isinstance(av,tuple):
                        if not attrs or (inout == 0 and not av[0] in attrs) \
                          or (inout == 1 and av[0] in attrs):
                            nattrs.append(tuple(av))
                    else:
                        nattrs.append(av)
                nv['attrs'] = nattrs
            nval.append(nv)
        if isinstance(l,dict):
            return nval[0]
        elif isinstance(l,tuple):
            return tuple(nval)
        else:
            return nval

    def _nl_get_links(self,request,resource,subpath_components):
        iproute = self._nl_get_iproute(netns=request.args.get('_netns',None))
        ret = []
        if len(subpath_components) > 1:
            raise PublicNlSdnError(
                "only one ifindex or interface name may be specified"
                " as the final path component")
        elif subpath_components and subpath_components[0] != '':
            try:
                ret = iproute.get_links(int(subpath_components[0]))
            except Exception:
                if subpath_components[0] == 'all':
                    ret = iproute.get_links('all')
                else:
                    indicies = iproute.link_lookup(ifname=subpath_components[0])
                    if indicies:
                        ret = iproute.get_links(*indicies)
        else:
            lookupargs = {}
            for (k,v) in iteritems(request.args):
                if not k in ['_netns','_filter','_keys','_attrs']:
                    # If the value is an int, convert.  That's all we
                    # handle; everything else must be a string.
                    try:
                        v = int(v)
                    except Exception:
                        pass
                    lookupargs[k] = v
            if lookupargs:
                indicies = iproute.link_lookup(**lookupargs)
                if indicies:
                    ret = iproute.get_links(*indicies)
            else:
                ret = iproute.get_links()

        keys = request.args.get('_keys',[])
        if isinstance(keys,list):
            keys = list(itertools.chain(*map(lambda x: x.split(','),keys)))
        else:
            keys = keys.split(',')
        attrs = request.args.get('_attrs',[])
        if isinstance(attrs,list):
            attrs = list(itertools.chain(*map(lambda x: x.split(','),attrs)))
        else:
            attrs = attrs.split(',')
        # Allow lowercase short-attr names as filters:
        nattrs = []
        for at in attrs:
            at = at.upper()
            if not at.startswith("IFLA_"):
                at = "IFLA_" + at
            nattrs.append(at)
        return self.__filter_nl_value(
            ret,request.args.get('_filter',0),keys,nattrs)

    def _nl_get_generic(self,request,resource,subpath_components):
        iproute = self._nl_get_iproute(netns=request.args.get('_netns',None))
        ret = []
        # NB: links are special and have a different interface.
        rdict = dict(
            routes=dict(attrprefix='RTA',getter=iproute.get_routes),
            addrs=dict(attrprefix='IFA',getter=iproute.get_addr),
            rules=dict(attrprefix='FRA',getter=iproute.get_rules),
            qdiscs=dict(attrprefix='TCA',getter=iproute.get_qdiscs),
            classes=dict(attrprefix='TCA',getter=iproute.get_classes),
            filters=dict(attrprefix='TCA',getter=iproute.get_filters),
            neighbors=dict(attrprefix='NDA',getter=iproute.get_neighbours),
            )
        if not resource in rdict:
            raise PublicNlSdnError("unknown resource %s" % (resource,))
        if subpath_components and subpath_components[0] != '':
            raise PublicNlSdnError(
                "%s resources may only be identified by args or data,"
                " not by path components" % (resource,))
        lookupargs = {}
        for (k,v) in iteritems(request.args):
            if not k in ['_netns','_filter','_keys','_attrs']:
                # If the value is an int, convert.  That's all we
                # handle; everything else must be a string.
                try:
                    v = int(v)
                except Exception:
                    pass
                lookupargs[k] = v
        ret = rdict[resource]['getter'](**lookupargs)

        keys = request.args.get('_keys',[])
        if isinstance(keys,list):
            keys = list(itertools.chain(*map(lambda x: x.split(','),keys)))
        else:
            keys = keys.split(',')
        attrs = request.args.get('_attrs',[])
        if isinstance(attrs,list):
            attrs = list(itertools.chain(*map(lambda x: x.split(','),attrs)))
        else:
            attrs = attrs.split(',')
        # Allow lowercase short-attr names as filters:
        nattrs = []
        attrprefix = rdict[resource]['attrprefix'].upper() + "_"
        for at in attrs:
            at = at.upper()
            if not at.startswith(attrprefix):
                at = attrprefix + at
            nattrs.append(at)
        #LOG.debug("ret = '%s'",str(ret))
        return self.__filter_nl_value(
            ret,request.args.get('_filter',0),keys,nattrs)

    def _nl_post_generic(self,request,resource,subpath_components):
        iproute = self._nl_get_iproute(netns=request.args.get('_netns',None))
        ret = []
        rdict = dict(
            links=dict(attrprefix='IFLA',f=iproute.link),
            routes=dict(attrprefix='RTA',f=iproute.route),
            addrs=dict(attrprefix='IFA',f=iproute.addr),
            rules=dict(attrprefix='FRA',f=iproute.rule),
            neighbors=dict(attrprefix='NDA',f=iproute.neigh),
            )
        if not resource in rdict:
            raise PublicNlSdnError("unknown resource %s" % (resource,))
        if subpath_components and subpath_components[0] != '':
            raise PublicNlSdnError(
                "adding %s must be done by POSTing data,"
                " not by path components" % (resource,))
        if not request.data:
            raise PublicNlSdnError(
                "adding %s requires a JSON dict of parameters"
                % (resource,))
        data = request.data
        if isinstance(data,basestring):
            try:
                data = json.loads(data)
            except Exception:
                raise PublicNlSdnError(
                    "adding %s requires a JSON dict of parameters"
                    % (resource,))
        if not isinstance(data,dict):
            raise PublicNlSdnError(
                "adding %s requires a JSON dict of parameters"
                % (resource,))

        ndata = {}
        for (k,v) in iteritems(data):
            if not k in ['_netns','_filter','_keys','_attrs']:
                ndata[k] = v
        try:
            ret = rdict[resource]['f']('add',**ndata)
            return None
        except Exception as e:
            raise PublicNlSdnError(e.__class__.__name__ + ": " + str(e))

        keys = request.args.get('_keys',[])
        if isinstance(keys,list):
            keys = list(itertools.chain(*map(lambda x: x.split(','),keys)))
        else:
            keys = keys.split(',')
        attrs = request.args.get('_attrs',[])
        if isinstance(attrs,list):
            attrs = list(itertools.chain(*map(lambda x: x.split(','),attrs)))
        else:
            attrs = attrs.split(',')
        # Allow lowercase short-attr names as filters:
        nattrs = []
        attrprefix = rdict[resource]['attrprefix'].upper() + "_"
        for at in attrs:
            at = at.upper()
            if not at.startswith(attrprefix):
                at = attrprefix + at
            nattrs.append(at)
        #LOG.debug("ret = '%s'",str(ret))
        return self.__filter_nl_value(
            ret,request.args.get('_filter',0),keys,nattrs)

    def _nl_delete_generic(self,request,resource,subpath_components):
        iproute = self._nl_get_iproute(netns=request.args.get('_netns',None))
        ret = []
        rdict = dict(
            links=dict(attrprefix='IFLA',f=iproute.link,
                       indexattr="ifindex",nameattr="ifname"),
            routes=dict(attrprefix='RTA',f=iproute.route),
            addrs=dict(attrprefix='IFA',f=iproute.addr),
            rules=dict(attrprefix='FRA',f=iproute.rule),
            neighbors=dict(attrprefix='NDA',f=iproute.neigh),
            )
        if not resource in rdict:
            raise PublicNlSdnError("unknown resource %s" % (resource,))
        data = {}
        if subpath_components and subpath_components[0] != '':
            v = subpath_components[0]
            try:
                v = int(v)
            except Exception:
                pass
            if isinstance(v,basestring) and not "nameattr" in rdict[resource]:
                raise PublicNlSdnError(
                    "cannot delete %s by name" % (resource,))
            elif isinstance(v,int) and not "indexattr" in rdict[resource]:
                raise PublicNlSdnError(
                    "cannot delete %s by index" % (resource,))
            else:
                raise PublicNlSdnError(
                    "cannot delete %s by path datatype '%s'"
                    % (resource,type(v)))

            if isinstance(v,basestring):
                data = { rdict[resource]["nameattr"]:v }
            elif isinstance(v,int):
                data = { rdict[resource]["indexattr"]:v }
        else:
            if not request.data:
                raise PublicNlSdnError(
                    "deleting %s requires a JSON dict of parameters"
                    % (resource,))
            rdata = request.data
            if isinstance(rdata,basestring):
                try:
                    rdata = json.loads(rdata)
                except Exception:
                    raise PublicNlSdnError(
                        "deleting %s requires a JSON dict of parameters"
                        % (resource,))
            if not isinstance(rdata,dict):
                raise PublicNlSdnError(
                    "deleting %s requires a JSON dict of parameters"
                    % (resource,))
            for (k,v) in iteritems(rdata):
                if not k in ['_netns','_filter','_keys','_attrs']:
                    data[k] = v
        try:
            ret = rdict[resource]['f']('delete',**data)
            return None
        except Exception as e:
            raise PublicNlSdnError(e.__class__.__name__ + ": " + str(e))

    def handle_nl_request(self,request,**kwargs):
        global r
        r = request
        ret = self.__validate_request(request,**kwargs)
        if ret:
            return ret

        pcl = request.subpath.split('/')
        resource = pcl[0]
        LOG.debug("%s %s",request.method,resource)

        nlmap = dict(
            links=dict(
                GET=self._nl_get_links,POST=self._nl_post_generic,
                DELETE=self._nl_delete_generic),
            routes=dict(
                GET=self._nl_get_generic,POST=self._nl_post_generic),
            addrs=dict(
                GET=self._nl_get_generic,POST=self._nl_post_generic),
            rules=dict(
                GET=self._nl_get_generic,POST=self._nl_post_generic),
            qdiscs=dict(
                GET=self._nl_get_generic),
            classes=dict(
                GET=self._nl_get_generic),
            filters=dict(
                GET=self._nl_get_generic),
            neighbors=dict(
                GET=self._nl_get_generic,POST=self._nl_post_generic),
            )

        # Figure out the resource and operation.
        (nlr,nlf) = (None,None)
        try:
            nlr = nlmap[resource]
        except Exception:
            return JsonErrorResponse(404,"invalid resource")
        try:
            nlf = nlr[request.method]
        except Exception:
            return JsonErrorResponse(404,"invalid operation")

        # Invoke the operation.
        try:
            ret = nlf(request,resource,pcl[1:])
            #LOG.debug("ret = '%s'",str(ret))
            resp = JsonResponse(200,ret)
            LOG.info("response: %r",resp)
            if len(resp.content) > 16:
                c = str(resp.content)[:16] + " ..."
            else:
                c = resp.content
            LOG.debug("content: %s",c)
            return resp
        except PublicNlSdnError as e:
            LOG.error("error while handling nl request")
            LOG.exception(sys.exc_info()[1])
            return JsonInternalServerErrorResponse(e.message)
        except Exception:
            LOG.error("error while handling nl request")
            LOG.exception(sys.exc_info()[1])
            return JsonInternalServerErrorResponse()

    def handle_nlsdn_request(self,request,**kwargs):
        global r
        r = request
        ret = self.__validate_request(request,**kwargs)
        if ret:
            return ret

        pcl = request.subpath.split('/')
        resource = pcl[0]
        LOG.debug("%s %s",request.method,resource)

        nlmap = dict(
            match=dict(
                GET=self.sdn.get_match,POST=self.sdn.add_match,
                DELETE=self.sdn.del_match),
            action=dict(
                GET=self.sdn.get_action,POST=self.sdn.add_action,
                DELETE=self.sdn.del_action),
            )

        # Figure out the resource and operation.
        (nlr,nlf) = (None,None)
        try:
            nlr = nlmap[resource]
        except Exception:
            return JsonErrorResponse(404,"invalid resource")
        try:
            nlf = nlr[request.method]
        except Exception:
            return JsonErrorResponse(405,"unsupported method")

        # Process arguments.  Resources may be listed (GET) or deleted
        # (DELETE) by id, and no other way.  Resources may be created by
        # POST, with a JSON dict of args used to construct the given
        # resource.
        (args,kwargs) = ([],{})
        if request.method in ["GET","DELETE"]:
            try:
                x = None
                if len(pcl) > 1:
                    x = int(pcl[1])
                elif request.method == "DELETE":
                    return JsonErrorResponse(400,"malformed request")
                if x != None:
                    if request.method == "GET":
                        kwargs["id"] = x
                    elif request.method == "DELETE":
                        args.append(x)
            except ValueError as e:
                if request.method in ["GET","DELETE"]:
                    return JsonErrorResponse(400,"invalid resource id")
                else:
                    LOG.error("error while handling nlsdn request")
                    LOG.exception(sys.exc_info()[1])
                    return JsonInternalServerErrorResponse()
            except Exception:
                LOG.error("error while handling nlsdn request")
                LOG.exception(sys.exc_info()[1])
                return JsonInternalServerErrorResponse()
        elif request.method == "POST":
            t = None
            try:
                if not request.data:
                    raise PublicNlSdnError(
                        "adding %s requires a JSON dict of parameters"
                        % (resource,))
                data = request.data
                if isinstance(data,basestring):
                    try:
                        data = json.loads(data)
                    except Exception:
                        raise PublicNlSdnError(
                            "adding %s requires a valid JSON dict of parameters"
                            % (resource,))
                if not isinstance(data,dict):
                    raise PublicNlSdnError(
                        "adding %s requires a JSON dict of parameters"
                        % (resource,))
                if not "type" in data:
                    raise PublicNlSdnError(
                        "adding %s requires a type (route, seg6) field"
                        % (resource,))
                tkwargs = data
                t = tkwargs["type"]
                del tkwargs["type"]
                #ndata = {}
                #for (k,v) in iteritems(data):
                #    if not k in ['_netns','_filter','_keys','_attrs']:
                #        ndata[k] = v
                if resource == "action":
                    args.append(
                        nlsdn.server.sdn.Engine.factory("action",t,**tkwargs))
                elif resource == "match":
                    args.append(
                        nlsdn.server.sdn.Engine.factory("match",t,**tkwargs))
                    #
                    # Also ensure that there is an action_id argument
                    # that points to an existing action.
                    #
                    if not "action_id" in tkwargs:
                        return JsonErrorResponse(400,"no action_id provided")
                    elif not self.sdn.get_action(id=tkwargs["action_id"]):
                        return JsonErrorResponse(
                            400,"action_id %s does not exist" % (str(tkwargs["action_id"])))
                else:
                    return JsonErrorResponse(
                        400,"invalid %s type" % (str(resource)))
            except PublicNlSdnError as e:
                LOG.error("error while handling nlsdn request")
                LOG.exception(sys.exc_info()[1])
                return JsonInternalServerErrorResponse(e.message)
            except Exception:
                LOG.error("error while handling nlsdn request")
                LOG.exception(sys.exc_info()[1])
                return JsonInternalServerErrorResponse()
        else:
            return JsonErrorResponse(405,"unsupported method")

        # Invoke the operation.
        try:
            ret = nlf(*args,**kwargs)
            if request.method == "GET" and ret == None:
                return JsonResponse(404,None)
            #LOG.debug("ret = '%s'",str(ret))
            resp = JsonResponse(200,ret)
            LOG.info("response: %r",resp)
            if len(resp.content) > 16:
                c = str(resp.content)[:16] + " ..."
            else:
                c = resp.content
            LOG.debug("content: %s",c)
            return resp
        except PublicNlSdnError as e:
            LOG.error("error while handling nl request")
            LOG.exception(sys.exc_info()[1])
            return JsonInternalServerErrorResponse(e.message)
        except Exception:
            LOG.error("error while handling nl request")
            LOG.exception(sys.exc_info()[1])
            return JsonInternalServerErrorResponse()
