
import os
import sys
import ipaddress
import json
import socket
from past.builtins import basestring
from future.utils import iteritems
from nlsdn.util import CustomJsonEncoder
from nlsdn.log import LOG
import pyroute2

class Resource(object):
    DB_KEYS = []
    NL_KEYS = []
    OTHER_KEYS = []

    @property
    def type(self):
        return None

    def to_nl(self):
        ret = dict()
        for k in self.__class__.NL_KEYS:
            v = getattr(self,k)
            if v is not None:
                ret[k] = v
        return ret

    def to_db(self):
        ret = self.to_nl()
        for k in self.__class__.DB_KEYS:
            v = getattr(self,k)
            if v is not None:
                ret[k] = v
        return ret

    to_json = to_db

    @classmethod
    def from_db(cls,d):
        #
        # We want to process all keys.  Any keys in NL_KEYS and
        # OTHER_KEYS are passed as args to our constructor.  Any keys in
        # DB_KEYS are set using setattr.  If there are any other keys,
        # we throw an error.
        #
        cargs = {}
        attrs = {}
        for (k,v) in iteritems(d):
            if k in cls.NL_KEYS or k in cls.OTHER_KEYS:
                cargs[k] = v
            elif k in cls.DB_KEYS:
                attrs[k] = v
            else:
                raise Exception("unrecognized key %s" % (str(k),))
        ret = cls(**cargs)
        for (k,v) in iteritems(attrs):
            setattr(ret,k,v)
        return ret

class Match(Resource):
    DB_KEYS = [ "type","action_id" ]
    NL_KEYS = [ "family" ]
    OTHER_KEYS = []

    def __init__(self,action_id=None,family=None):
        self.action = action_id
        self.family = family

    def __contains__(self, item):
        """
        A match is defined to be "in" this match if the values of the other's keys are either equal to or "in" this
        match.
        If a key in this object and the other is undefined, it does not contribute to being "in"
        If a key is defined in this object but not the other, the other's key is considered "in"
        If a key is not defined in this object but is in the other, the other is considered not "in"
        """
        mtype = getattr(self, 'type')
        otype = getattr(item, 'type')
        if not mtype == otype:
            # Maybe there is a sane way to compare different-type things, but I don't want to think about it
            return False

        for key in self.NL_KEYS:
            # This could be more clearly written as some kind of filter, but the idea is to look at
            # every key. If the key makes the comparison "not in", return False immediately, otherwise
            # keep looking at the rest of the keys. If we run out of keys and have not exited with False,
            # return True
            if key in ['src_len', 'dst_len']:
                # These get used as part of comparing src and dst, so skip here
                continue

            mval = getattr(self, key)
            oval = getattr(item, key)

            if oval is None:
                # An unspecified key in the other is always acceptable
                continue
            elif mval is None:
                # The key is not defined in this object but is in the other, which means the other is
                # more specific and "not in"
                return False

            if key in ['src', 'dst']:
                # IP address support subnet comparison
                mlen = str(getattr(self, key + '_len'))
                olen = str(getattr(item, key + '_len'))

                maddr = ipaddress.ip_interface(mval + "/" + mlen)
                oaddr = ipaddress.ip_interface(oval + "/" + olen)

                # Check if the other network is a subnet of this network
                if oaddr.ip in maddr.network and oaddr.network.prefixlen >= maddr.network.prefixlen:
                    continue
                else:
                    return False

            # All other values are primitives, so do not support the "in" operator. Check equality
            if mval == oval:
                continue
            else:
                return False
        return True

class RuleMatch(Match):
    DB_KEYS = list(Match.DB_KEYS)
    DB_KEYS.extend(['action_id'])
    NL_KEYS = list(Match.NL_KEYS)
    NL_KEYS.extend([
        'priority','src','dst','src_len','dst_len','iifname','oifname',
        'ipproto','sport_range','dport_range'
    ])
    OTHER_KEYS = [ "action_id" ]

    def __init__(self,family=socket.AF_INET,priority=None,
                 src=None,dst=None,src_len=None,dst_len=None,
                 iifname=None,oifname=None,ipproto=None,sport_range=None,dport_range=None,
                 action_id=None):
        super(RuleMatch,self).__init__(family=family)
        self.priority = priority
        self.src = src
        self.dst = dst
        self.src_len = src_len
        self.dst_len = dst_len
        self.iifname = iifname
        self.oifname = oifname
        self.ipproto = ipproto
        self.sport_range = sport_range
        self.dport_range = dport_range
        self.action_id = action_id

    @property
    def type(self):
        return "rule"

    def __repr__(self):
        return "RuleMatch(%s,%s/%s,%s/%s,%s,%s,%s,%s,%s->%s)" \
            % (str(self.priority),str(self.src),str(self.src_len),
               str(self.dst),str(self.dst_len),
               str(self.iifname),str(self.oifname),
               str(self.ipproto),str(self.sport_range),str(self.dport_range),
               str(self.action_id))

class NftablesRuleMatch(Match):
    """
    Marks a packet using an nftables rule.  The nftables rule sets the mark (and mangles tos/dsfield if specified) for matching packets; then the rule uses the mark to direct to a routing table.
    """
    DB_KEYS = list(Match.DB_KEYS)
    DB_KEYS.extend(['action_id'])
    NL_KEYS = list(Match.NL_KEYS)
    NL_KEYS.extend([
        'priority',
        'src','dst','src_len','dst_len',
        'dscp','new_dscp'
    ])
    OTHER_KEYS = [ "action_id" ]

    def __init__(self,family=socket.AF_INET,priority=None,
                 src=None,dst=None,src_len=None,dst_len=None,
                 dscp=None,new_dscp=None,
                 action_id=None):
        super(NftablesRuleMatch,self).__init__(family=family)
        self.priority = priority
        self.src = src
        self.dst = dst
        self.src_len = src_len
        self.dst_len = dst_len
        self.dscp = dscp
        self.new_dscp = new_dscp
        self.action_id = action_id

    @property
    def type(self):
        return "nftablesrule"

    def __repr__(self):
        return "NftablesRuleMatch(%s,%s/%s,%s/%s,%s/%s->%s)" \
            % (str(self.priority),
               str(self.src),str(self.src_len),
               str(self.dst),str(self.dst_len),
               str(self.dscp),str(self.new_dscp),
               str(self.action_id))

class Action(Resource):
    DB_KEYS = [ "matches","type" ]
    NL_KEYS = [ "family" ]
    OTHER_KEYS = []

    def __init__(self,family=None, matches=None):
        self.matches = {} if matches is None else matches
        self.family = family

    def add_match(self,match_id):
        self.matches[str(match_id)] = match_id

    def del_match(self,match_id):
        del self.matches[str(match_id)]

class RouteAction(Action):
    """
    Each route action is modeled as a table.  Multiple matches may feed
    a single action.
    """
    DB_KEYS = Action.DB_KEYS
    NL_KEYS = Action.NL_KEYS
    NL_KEYS.extend([
        "number","dst","dst_len","gateway","oif"
    ])
    OTHER_KEYS = []

    def __init__(self,number=None,dst=None,dst_len=None,gateway=None,oif=None,family=socket.AF_INET, matches=None):
        super(RouteAction,self).__init__(family=family, matches=matches)
        self.number = number
        self.dst = dst
        self.dst_len = dst_len
        self.gateway = gateway
        self.oif = oif

    @property
    def type(self):
        return "route"

    def __repr__(self):
        return "Action(%s,%s/%d,%s,%s)" % (
            str(self.number),self.dst,int(self.dst_len),self.gateway,self.oif)

class Seg6Action(RouteAction):
    """
    Each ipv6 segment route action is modeled as a single route table.  It
    corresponds to
      ip route add dst/dst_len encap seg6 mode encap segs x,y,z
    depending on the particular route options you specify.  NB: you must
    specify `encap['segs']` in the constructor, but it will default to an
    encapsulation mode (`encap['mode']`) of `encap` if you don't specify
    otherwise (other options are `inline` and `l2encap`).
    """
    DB_KEYS = RouteAction.DB_KEYS
    NL_KEYS = RouteAction.NL_KEYS
    NL_KEYS.extend([
        "encap"
    ])
    OTHER_KEYS = []

    def __init__(self,number=None,dst=None,dst_len=None,gateway=None,oif=None,
                 family=socket.AF_INET,matches=None,encap={}):
        if not encap:
            encap = {}
        if 'mode' in encap \
          and not encap['mode'] in ["inline","encap","l2encap"]:
            raise Exception("invalid seg6 encap[mode]")
        else:
            encap['mode'] = 'inline'
        if not 'segs' in encap or not encap['segs']:
            raise Exception("no seg6 tunnel segs supplied")
        if 'type' in encap and encap['type'] != 'seg6':
            raise Exception("only encap[type] == seg6 supported")
        if not 'type' in encap:
            encap['type'] = 'seg6'
        super(Seg6Action, self).__init__(
            number=number,dst=dst,dst_len=dst_len,gateway=gateway,oif=oif,family=family,matches=matches)
        self.encap = encap

    @property
    def type(self):
        return "seg6"

    def __repr__(self):
        return "Seg6Action(%s,%s/%s,%s,%s)" \
            % (str(self.number),str(self.dst),str(self.dst_len),str(self.oif),
               str(self.encap))

class MultiAction(Action):
    def __init__(self,number=None):
        raise Exception("not implemented")

class Engine(object):
    def __init__(self,iproute=None,path=None,
                 rule_range=(1,32765),action_range=(256,32768)):
        self.iproute = iproute or pyroute2.IPRoute()
        self.path = path
        (self.match_min,self.match_max) = (rule_range[0],rule_range[1])
        (self.action_min,self.action_max) = (action_range[0],action_range[1])
        self.db = dict(
            action={},match={},idtable=dict(action=0,match=0))
        self._loaded = False

    @staticmethod
    def factory(resource,action,**kwargs):
        if resource == "action" and action == "seg6":
            return Seg6Action(**kwargs)
        elif resource == "action" and action == "route":
            return RouteAction(**kwargs)
        elif resource == "match" and action == "rule":
            return RuleMatch(**kwargs)
        elif resource == "match" and action == "nftablesrule":
            return NftablesRuleMatch(**kwargs)
        elif not resource in [ "action","match" ]:
            raise Exception("unknown resource type %s" % (str(resource),))
        else:
            raise Exception("unknown %s type %s"
                            % (str(resource),str(action)))

    @staticmethod
    def _db_factory(resource,action,**kwargs):
        if resource == "action" and action == "seg6":
            return Seg6Action.from_db(kwargs)
        elif resource == "action" and action == "route":
            return RouteAction.from_db(kwargs)
        elif resource == "match" and action == "rule":
            return RuleMatch.from_db(kwargs)
        elif resource == "match" and action == "nftablesrule":
            return NftablesRuleMatch.from_db(kwargs)
        elif not resource in [ "action","match" ]:
            raise Exception("unknown resource type %s" % (str(resource),))
        else:
            raise Exception("unknown %s type %s"
                            % (str(resource),str(action)))

    def load(self,force=False,ensure=True):
        if self._loaded and not force:
            return
        raw = ""
        if os.path.exists(self.path):
            fd = open(self.path,'r')
            while True:
                x = fd.read()
                if not x:
                    break
                raw += x
            fd.close()
        rdb = {}
        if raw != "":
            rdb = json.loads(raw)
        if "idtable" in rdb:
            self.db["idtable"] = rdb["idtable"]
        if "action" in rdb:
            for k in sorted(rdb["action"].keys()):
                v = rdb["action"][k]
                atype = v["type"]
                del v["type"]
                privattrs = {}
                for (ki,vi) in iteritems(v):
                    if ki.startswith("_"):
                        privattrs[ki] = vi
                        del v[ki]
                ov = Engine._db_factory("action",atype,**v)
                for (ki,vi) in iteritems(privattrs):
                    setattr(ov,ki,vi)
                self.db["action"][k] = ov
        if "match" in rdb:
            for k in sorted(rdb["match"].keys()):
                v = rdb["match"][k]
                mtype = v["type"]
                del v["type"]
                ov = Engine._db_factory("match",mtype,**v)
                self.db["match"][k] = ov
        self._loaded = True

    def apply(self):
        """
        Ensure everything in our DB exists in the local router.
        """
        raise Exception("not implemented")

    def store(self):
        fd = open(self.path,'w')
        fd.write(json.dumps(self.db,indent=4,sort_keys=True,
                            cls=CustomJsonEncoder))
        fd.close()

    def __get_next_id(self,x):
        if not x in self.db['idtable']:
            raise Exception("no such index %s" % (str(x),))
        ret = self.db['idtable'][x]
        min = getattr(self,x+"_min")
        max = getattr(self,x+"_max")
        if ret > (max - min):
            raise Exception("no more %s available" % (str(x),))
        # Find the next one, might be out of range:
        n = ret + 1
        while True:
            if n in self.db[x]:
                n += 1
            else:
                self.db['idtable'][x] = n
                break
        return ret

    def get_action(self,id=None):
        if id != None:
            if not str(id) in self.db["action"]:
                return None
            else:
                return self.db["action"][str(id)]
        else:
            return self.db["action"]

    def add_action(self,action):
        if not isinstance(action,Action):
            raise Exception("invalid action!")
        old_next_id = self.db['idtable']['action']
        if not action.number:
            action.number = self.__get_next_id("action")
        elif str(action.number) in self.db["action"]:
            raise Exception("action %s already exists" % (str(action.number),))
        elif action.number == old_next_id:
            # Kick the counter to the next if the user asked for the
            # next; else, they requested a higher index, so don't move
            # the counter.
            self.__get_next_id("action")
        # Add to DB first, then to netlink
        self.db["action"][str(action.number)] = action
        self.store()
        # To netlink:
        table = action.number+self.action_min
        try:
            args = action.to_nl()
            if isinstance(action,Seg6Action):
                seg = args['encap']['segs'][0]
            LOG.debug("iproute.route add: %s",str(args))
            self.iproute.route(
                'add',table=table,**args)
            # NB: if this is a seg6action, for the inline case at least, we
            # need to add a specific route to the first segment, since we don't
            # apparently get looked up outside the table we're in.  So find a
            # route (in the main table) to that destination via the oif we were
            # given.
            if isinstance(action,Seg6Action):
                #seg = args['encap']['segs'][0]
                rl = self.iproute.get_routes(table=254,family=action.family,oif=action.oif)
                if rl:
                    sargs = dict(table=table,family=action.family,
                                 dst=seg,dst_len=128,oif=action.oif,
                                 gateway=rl[0].get_attr('RTA_GATEWAY'))
                    LOG.debug("seg: %s",str(seg))
                    LOG.debug("encap: %s",str(action.encap))
                    LOG.debug("iproute.route add: %s",str(sargs))
                    self.iproute.route('add',**sargs)
                else:
                    LOG.warning("no route available to first segment in %r; skipping!",action)
            self.store()
        except Exception:
            e = sys.exc_info()[1]
            LOG.exception(e)
            # Remove from DB:
            self.db['idtable']['action'] = old_next_id
            del self.db["action"][str(action.number)]
            self.store()
            raise Exception("failed to add action: %s" % (str(e)))
        # Success
        return action.number

    def del_action(self,number):
        numstr = str(number)
        if not numstr in self.db["action"]:
            raise Exception("nonexistent action %s" % (numstr,))
        action = self.db["action"][numstr]
        if action.matches:
            raise Exception("matches still depend on action %s; cannot delete"
                            % (str(number),))
        # Try to remove from netlink:
        try:
            args = action.to_nl()
            LOG.debug("iproute.route del: %s",str(args))
            self.iproute.route(
                'del',table=action.number+self.action_min,**args)
            LOG.debug("flushing table %s",
                      str(self.action_min+action.number))
            self.iproute.flush_routes(table=self.action_min+action.number)
        except Exception:
            e = sys.exc_info()[1]
            LOG.exception(e)
            raise Exception("failed to remove action: %s" % (str(e)))
        del self.db["action"][numstr]
        if number < self.db["idtable"]["action"]:
            self.db["idtable"]["action"] = number
        self.store()

    def get_match(self,id=None):
        if id is not None:
            if not str(id) in self.db["match"]:
                return None
            else:
                return self.db["match"][str(id)]
        else:
            return self.db["match"]

    def add_match(self,match):
        if not isinstance(match,Match):
            raise Exception("invalid match!")
        if not str(match.action_id) in self.db["action"]:
            raise Exception(
                "action_id %s does not exist"% (str(match.action_id)))
        old_next_id = self.db['idtable']['match']
        if not match.priority:
            match.priority = self.__get_next_id("match")
        elif str(match.priority) in self.db["match"]:
            raise Exception("match %s already exists" % (str(match.priority),))
        elif match.priority == old_next_id:
            # Kick the counter to the next if the user asked for the
            # next; else, they requested a higher index, so don't move
            # the counter.
            self.__get_next_id("match")
        # Add to DB first, then to netlink
        self.db["match"][str(match.priority)] = match
        self.store()
        # To netlink:
        try:
            table = self.db["action"][str(match.action_id)].number \
                + self.action_min
            if isinstance(match,RuleMatch):
                args = match.to_nl()
                priority = match.priority + self.match_min
                args["priority"] = priority
                args["table"] = table
                if "sport_range" in args and args["sport_range"] and not isinstance(args["sport_range"],basestring):
                    args["sport_range"] = "%s:%s" % (str(args["sport_range"][0]),str(args["sport_range"][1]))
                if "dport_range" in args and args["dport_range"] and not isinstance(args["dport_range"],basestring):
                    args["dport_range"] = "%s:%s" % (str(args["dport_range"][0]),str(args["dport_range"][1]))
                LOG.debug("iproute.rule add: %s",str(args))
                self.iproute.rule('add',**args)
            elif isinstance(match,NftablesRuleMatch):
                # Because of the pain of deleting nft rules (you have to lookup
                # after creation and get the handle), and because of the
                # corresponding pain of inserting into <position> prior to the
                # handle you want to precede (e.g. the kernel maintains no
                # sort), we just create a chain per match with this match's
                # priority.  Then we delete rules by deleting the chain.
                #
                # We need to invert prio, to ensure we are below the -500
                # mangle netfilter prio.
                nft_priority = -500 - self.match_max + match.priority
                priority = self.match_min + match.priority
                chain_name = "c-%d" % (priority,)
                #chain_name = "nlsdn"
                nft = pyroute2.NFTables(nfgen_family=match.family)
                from pyroute2.nftables.expressions import ipvXaddr,mark,counter,dscp,notrack,l4proto
                try:
                    nft.table('add',name="nlsdn")
                except Exception:
                    pass
                try:
                    nft.chain('add',table="nlsdn",name=chain_name,
                              type="filter",hook="prerouting",
                              policy=1,priority=nft_priority)
                except Exception:
                    pass
                try:
                    exprs = [
                        l4proto(chr(89),cmp_op=1),
                        #mark(match=0),
                        dscp(match=0,family=socket.AF_INET6)
                    ]
                    akwargs = {}
                    if match.dscp:
                        exprs.append(dscp(match=match.dscp,family=match.family))
                    if match.src_len > 0 \
                      and not (match.src_len == 128 and match.family == socket.AF_INET6) \
                      and not (match.src_len == 32 and match.family == socket.AF_INET):
                        akwargs['src'] = "%s/%s" % (match.src,match.src_len)
                    elif match.src:
                        akwargs['src'] = match.src
                    if akwargs:
                        exprs.append(ipvXaddr(family=match.family,**akwargs))
                    akwargs = {}
                    if match.dst_len > 0 \
                      and not (match.dst_len == 128 and match.family == socket.AF_INET6) \
                      and not (match.dst_len == 32 and match.family == socket.AF_INET):
                        akwargs['dst'] = "%s/%s" % (match.dst,match.dst_len)
                    elif match.dst:
                        akwargs['dst'] = match.dst
                    if akwargs:
                        exprs.append(ipvXaddr(family=match.family,**akwargs))
                    exprs.append(mark(new=priority))
                    if match.new_dscp:
                        exprs.append(dscp(new=match.new_dscp,family=match.family))
                    exprs.append(counter())
                    exprs.append(notrack())
                    nft.rule('add',table="nlsdn",chain=chain_name,expressions=exprs)
                    self.iproute.rule('add',table=table,priority=priority,fwmark=priority,
                                      family=match.family)
                except Exception:
                    e = sys.exc_info()[1]
                    LOG.exception(e)
            self.db["action"][str(match.action_id)].add_match(match.priority)
            self.store()
        except Exception:
            e = sys.exc_info()[1]
            LOG.exception(e)
            # Remove from DB:
            self.db['idtable']['match'] = old_next_id
            del self.db["match"][str(match.priority)]
            self.store()
            raise Exception("failed to add match: %s" % (str(e)))
        # Success
        return match.priority

    def del_match(self,priority):
        prioritystr = str(priority)
        if not prioritystr in self.db["match"]:
            raise Exception("nonexistent match %s" % (prioritystr,))
        match = self.db["match"][prioritystr]
        # Try to remove from netlink:
        try:
            table = self.db["action"][str(match.action_id)].number \
                + self.action_min
            if isinstance(match,RuleMatch):
                args = match.to_nl()
                args["priority"] = self.match_min+match.priority
                self.iproute.rule('del',**args)
            elif isinstance(match,NftablesRuleMatch):
                # Because of the pain of deleting nft rules (you have to lookup
                # after creation and get the handle), and because of the
                # corresponding pain of inserting into <position> prior to the
                # handle you want to precede (e.g. the kernel maintains no
                # sort), we just create a chain per match with this match's
                # priority.  Then we delete rules by deleting the chain.
                #
                # We need to invert prio, to ensure we are below the -500
                # mangle netfilter prio.
                nft_priority = -500 - self.match_max + match.priority
                priority = self.match_min + match.priority
                chain_name = "c-%d" % (priority,)
                nft = pyroute2.NFTables(nfgen_family=match.family)
                nft.chain('del',table="nlsdn",name=chain_name)
                self.iproute.rule('del',table=table,priority=priority,fwmark=priority,
                                  family=match.family)
        except Exception:
            e = sys.exc_info()[1]
            LOG.exception(e)
            raise Exception("failed to remove match: %s" % (str(e)))
        del self.db["match"][prioritystr]
        if priority < self.db["idtable"]["match"]:
            self.db["idtable"]["match"] = priority
        try:
            self.db["action"][str(match.action_id)].del_match(match.priority)
        except Exception:
            LOG.error("BUG: match %s action %s already deleted!",
                      str(match),str(match.action_id))
            LOG.exception(sys.exc_info()[1])
        self.store()
