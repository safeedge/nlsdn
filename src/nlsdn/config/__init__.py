
import os
import json
from nlsdn.log import LOG

DEFAULT_CONFIGFILE_PATHS = [ "/etc/nlsdn/config.json" ]
if os.getenv("HOME"):
    DEFAULT_CONFIGFILE_PATHS.insert(
        0,"%s/.nlsdn/config.json" % (os.getenv("HOME")))
if os.getenv("NLSDN_CONFIGFILE"):
    DEFAULT_CONFIGFILE_PATHS = [ os.getenv("NLSDN_CONFIGFILE") ]

def find_configfile():
    for fp in DEFAULT_CONFIGFILE_PATHS:
        if os.path.exists(fp) and os.access(fp,os.R_OK):
            return fp
    return None

def add_default_configfile(fp):
    global DEFAULT_CONFIGFILE_PATHS
    if not fp in DEFAULT_CONFIGFILE_PATHS:
        DEFAULT_CONFIGFILE_PATHS.insert(0,fp)
    return None

CONFIG_SECTIONS = {}
CONFIG_SECTION_ORDER = []

def global_config_section(cls):
    """
    A simple decorator that "registers" classes as top-level ConfigSections
    for our Config object.
    """
    global CONFIG_SECTIONS
    csname = cls.name
    if csname in CONFIG_SECTIONS:
        raise RuntimeError("section '%s' already globally registered!" % (csname))
    CONFIG_SECTIONS[csname] = cls
    CONFIG_SECTION_ORDER.append(csname)
    return cls

class ConfigError(Exception):
    pass

class ConfigSection(object):
    """
    A ConfigSection processes a chunk of the json config file, and is
    "applied" to an AuthServer instance, once parsed and validated.
    Usually, a ConfigSection apply() invocation would return either an
    Authn instance, an Authz instance, or a 2-tuple of (Authn,Authz)
    instances (in the case that one backend provides both authn and
    authz.  apply() may also return None; perhaps it configures a
    customized Server.
    """
    name = None
    """The json key for this section"""
    reconfig = False
    """True if this section supports online reconfiguration; False otherwise"""

    def __init__(self,configdir,jsonblob={}):
        self.configdir = configdir
        self._json = jsonblob
        self._hash = hash(
            json.dumps(jsonblob,sort_keys=True,separators=(',',':')))
        return

    def __eq__(self,other):
        """
        :returns: True if :param self: is identical to :param other: ; False otherwise.
        """
        return type(self) == type(other) and self._hash == other._hash

    def __ne__(self,other):
        """
        :returns: True if self differs from :param other: ; False otherwise.
        """
        return not self.__eq__(other)

    def dump(self,full=False,indent=0):
        return repr(self)

class Config(object):
    """
    An object containing any parsed `ConfigSection` objects.

    Each ConfigSection must stand by itself, depending on no other
    ConfigSections.  This restriction is to allow online reconfiguration
    of an existing server, given that we allow some sections to prevent
    their reconfiguration.  For instance, suppose the user edits the
    config file for a running server, and changes the server host/port
    AND adds a new static user.  We want to pick up the new static user,
    but we will not restart the server on a new port.  Thus, in that
    case, we need to be able to graft the old, unmodifiable sections of
    the old config with the modified (or modifiable) sections of the new
    config.  Thus each config section must be independent.
    """
    def __init__(self,configfile=find_configfile(),sections={},order=[]):
        """
        Parses :param configfile:, either according to :param sections:
        and :param order: (although if sections is specified and order
        is not, order will be automatically created as sections.keys()),
        or to the global CONFIG_SECTIONS and CONFIG_SECTION_ORDER vars
        that are updated by the `global_config_section` decorator.  The
        fields resulting from the parsed config are set (by section
        name) in this object.  Any state object
        returned by `section.apply()` is stored as generic state in the
        server as well.
        """
        self.configfile = configfile
        st = os.stat(self.configfile)
        self.timestamp = st.st_mtime
        fd = open(self.configfile,'r')
        contents = fd.read()
        fd.close()
        self.configdir = os.path.dirname(self.configfile)
        jsonblob = json.loads(contents)
        if sections:
            self.sections = sections
            self.order = order or self.sections.keys()
        else:
            self.sections = CONFIG_SECTIONS
            self.order = CONFIG_SECTION_ORDER
        
        for section in self.order:
            blob = {}
            if section in jsonblob:
                blob = jsonblob[section]
            if not section in self.sections:
                raise ConfigError("section '%s' specified in ordering, but no class"
                                  " specified to process it!" % (section))
            ns = self.sections[section](self.configdir,blob)
            setattr(self,section,ns)
            LOG.debug("config section: %s",ns.dump())

    def is_stale(self):
        """Check if the config file has been updated since last parse."""
        st = os.stat(self.configfile)
        if self.timestamp < st.st_mtime:
            return st.st_mtime
        return False

    def reload(self):
        """Reload the config file.  :returns: a new Config instance."""
        return Config(self.configfile,sections=self.sections,order=self.order)

    def dump(self,full=False):
        retval = ""
        for section in self.order:
            if hasattr(self,section):
                st = getattr(self,section).dump(indent=4)
                retval += "  Section '%s':\n    %s\n" % (section,st)
        return retval
