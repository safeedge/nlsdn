#!/bin/env python

import sys
import ssl
from flask import Flask,request,make_response
import nlsdn.log
from nlsdn.log import LOG
from nlsdn.config import ( Config,find_configfile )
from nlsdn.server.handlers import NlSdnServer
from nlsdn.server.http import NlSdnRequest

#
# Initialize our Flask app immediately, so we can log during config
# processing.  Use "application" so this can trivially be run from an
# WSGI environment.
#
application = Flask('nlsdn')
nlsdn.log.setLogger(application.logger)

server = None
config = None
args = None

def convert_flask_request(request,subpath):
    """
    Convert a Flask :class:`flask.Request` object to a
    :class:`nlsdn.server.http.NlSdnRequest` object.
    
    :param request: a Flask :class:`flask.Request` object.
    :ptype request: :class:`flask.Request`
    :returns: a :class:`nlsdn.server.http.NlSdnRequest` object.
    """
    return NlSdnRequest(
        subpath,request.method,request.url,request.remote_addr,
        args=request.args,data=request.data,headers=request.headers,
        environ=dict(request.environ),framework_request_obj=request)

def make_request_handler(url,name,handler):
    LOG.debug("adding endpoint %s (%s, %s)",name,url,str(handler))
    def __handler(subpath):
        LOG.debug("__handler %s",name)
        response = handler(convert_flask_request(request,subpath))
        return make_response(response.content,response.code,response.headers)
    return __handler

def main():
    """
    The Flask standalone entrypoint.
    """
    global server, config, args
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("-f","--config-file",default=find_configfile())
    args = parser.parse_args()

    config = Config(args.config_file)
    server = NlSdnServer(config,standalone=True)
    LOG.info("created standalone NlSdnServer")
    # Use an explicit registration instead of an @application.route
    # decorator so we can allow the endpoint URL to be configured.
    # Use a dynamically-built handler so we can invoke different handler
    # methods on the server for different URL endpoints.
    for (url,name,handler,methods) in server.endpoints:
        url += "/<path:subpath>"
        application.add_url_rule(
            url,name,make_request_handler(url,name,handler),methods=methods)

    if not server.config.tls.server_privkey_file \
       or not server.config.tls.server_cert_file:
       LOG.warning("no TLS configuration specified; should be used in standalone mode!")
       ctx = None
    else:
        ctx = ssl.create_default_context(purpose=ssl.Purpose.CLIENT_AUTH)
        ctx.load_verify_locations(cafile=config.tls.ca_cert_file)
        ctx.load_cert_chain(config.tls.server_cert_file,
                            keyfile=config.tls.server_privkey_file,
                            password=config.tls.server_privkey_passphrase)
        ctx.verify_mode = ssl.CERT_OPTIONAL

    server.drop_privileges()

    (host,port) = (server.config.server.host,server.config.server.port)
    LOG.info("starting standalone on host %s port %s",host,str(port))
    application.run(host=host,port=port,threaded=server.config.server.threaded,
                    ssl_context=ctx)
    sys.exit(1)

if __name__ == '__main__':
    main()

def wsgi():
    """
    The Flask WSGI entrypoint (really, anything where the Flask server
    is not running in standalone mode, i.e. is not the HTTP server).

    :returns: the application object.
    """
    global server
    config = Config(find_configfile())
    server = NlSdnServer(config)

    # Use an explicit registration instead of an @application.route
    # decorator so we can allow the endpoint URL to be configured.
    for (url,name,handler) in server.endpoints:
        url += "/<path:subpath>"
        application.add_url_rule(
            url,name,make_request_handler(url,name,handler),methods=methods)

    LOG.info("created WSGI NlSdnServer")

    server.drop_privileges()

    #
    # Return the application to our caller, who must set
    # __main__.application so that things like mod_wsgi work.
    #
    return application
