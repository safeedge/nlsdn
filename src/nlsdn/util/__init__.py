
import builtins
import time
import json
import pyroute2

def isiterable(x):
    """
    :param x: any Python value.
    :returns: True if ``x`` is iterable; False otherwise.
    """
    try:
        iter(x)
        return True
    except Exception:
        return False

def utcoffset():
    """
    Return the offset from local time to UTC time, in seconds.
    """
    if time.daylight:
        return time.altzone
    else:
        return time.timezone

def parse_bool_or_int(str):
    if not str:
        return False
    str = str.lstrip(' ').rstrip(' ')
    if str in ['True','TRUE','true','T','t','yes','YES','Yes','Y','y']:
        return True
    elif str in ['False','FALSE','false','F','f','no','NO','No','N','n']:
        return False
    else:
        return int(str)

class CustomJsonEncoder(json.JSONEncoder):
    def default(self,obj):
        if isinstance(obj,pyroute2.netlink.nla_slot):
            return tuple(obj)
        elif hasattr(builtins,"unicode") and isinstance(obj,builtins.unicode):
            return str(obj)
        elif hasattr(obj,'to_json') and callable(obj.to_json):
            return obj.to_json()
        else:
            return json.JSONEncoder.default(self,obj)
