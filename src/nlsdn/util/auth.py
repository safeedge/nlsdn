from cryptography.hazmat.primitives import hashes
from cryptography.x509 import load_pem_x509_certificate
from cryptography.hazmat.backends import default_backend

from nlsdn.config import ConfigError

class Cert(object):
    def __init__(self,cert):
        self.cert = cert
        self.fingerprint = cert.fingerprint(hashes.SHA256())
        self.fingerprint_str = None
        for byte in self.fingerprint:
            if self.fingerprint_str is None:
                self.fingerprint_str = "%.2x" % (ord(byte))
            else:
                self.fingerprint_str += ":%.2x" % (ord(byte))

    @classmethod
    def fromfile(cls,filepath):
        fd = open(filepath,'rb')
        contents = fd.read()
        fd.close()
        return cls.fromstring(str(contents))

    @classmethod
    def fromstring(cls,contents):
        return Cert(load_pem_x509_certificate(contents,default_backend()))

    def match(self,fingerprint):
        return self.fingerprint == fingerprint

    def __repr__(self):
        return "Cert(%s=%r)" % (self.fingerprint_str,self.cert)

class CertBundle(object):
    def __init__(self,cert_list):
        self.certs = {}
        for cert in cert_list:
            if not isinstance(cert,Cert):
                raise ConfigError("'%s' not a Cert!" % (str(cert)))
            self.certs[cert.fingerprint] = cert

    @classmethod
    def fromfile(cls,filepath):
        fd = open(filepath,'rb')
        contents = fd.read()
        fd.close()
        return cls.fromstring(str(contents))

    @classmethod
    def fromstring(cls,contents):
        nlstart = 0
        if nlstart < 0:
            return CertBundle([])
        nlend = contents.find('\n',nlstart+1)
        lst = []
        certstart = -1
        while nlstart > -1 and nlend > -1:
            if certstart > -1 \
              and contents.find('-END CERTIFICATE-',nlstart,nlend) > -1:
                certcontent = contents[certstart:nlend]
                cert = Cert(load_pem_x509_certificate(certcontent,
                                                      default_backend()))
                lst.append(cert)
                certstart = -1
            elif contents.find('-BEGIN CERTIFICATE-',nlstart,nlend) > -1:
                certstart = nlstart
            nlstart = nlend
            nlend = contents.find('\n',nlstart+1)
        return CertBundle(lst)

    @classmethod
    def bundle_or_cert_fromfile(cls,filepath):
        bundle = cls.fromfile(filepath)
        sz = bundle.size()
        if sz == 0:
            return None
        elif sz == 1:
            return bundle.certs.values()[0]
        else:
            return bundle

    def size(self):
        return len(self.certs)

    def match(self,fingerprint):
        return fingerprint in self.certs

    def __iter__(self):
        return self.certs.values().__iter__()

    def __repr__(self):
        return "CertBundle(%s)" % (str(list(map(lambda x: repr(x),
                                                self.certs.values()))))
